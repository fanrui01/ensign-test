import React from 'react';
import { Jumbotron } from 'reactstrap';

const TopPart = (props) => {

    return (
        <div>
            <Jumbotron>
                <h1 className="display-3"> { props.title } </h1>
                <p className="lead"> { props.description } </p>
                <hr className="my-2" />
                <p> { props.paragraph } </p>
                <p className="lead">
                {/* <Button color="primary"></Button> */}
                </p>
            </Jumbotron>
        </div>
    );

};
export default TopPart;