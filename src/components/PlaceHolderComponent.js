import React from 'react';
import { Container } from 'reactstrap';

const PlaceHolder = (props) => {
    return(
        <Container>
            <h1> PlaceHolder </h1>
        </Container>
    );
}

export default PlaceHolder;   