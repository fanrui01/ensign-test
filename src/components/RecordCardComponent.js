import React from 'react';
import {
  Card, CardText, CardBody,
  CardTitle, CardSubtitle, Button
} from 'reactstrap';

const RecordCard = (props) => {
    return (
        <div>
            <Card>
                <CardBody>
                    <CardTitle> { props.title }</CardTitle>
                    <CardSubtitle> { props.subtitle } </CardSubtitle>
                    <CardText> { props.text } </CardText>
                    <Button onClick={ props.button_func_1 } value={ props.button_value_1 } color={props.button_color_1 } > { props.button_text_1 } </Button>
                    <Button onClick={ props.button_func_2 } value={ props.button_value_2 } color={props.button_color_2 } > { props.button_text_2 } </Button>
                </CardBody>
            </Card>
        </div>
    );
}

export default RecordCard;