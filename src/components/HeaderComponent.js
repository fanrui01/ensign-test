import React, { Fragment, Component } from 'react';

import { Container, Nav, NavItem, Navbar, NavbarBrand,
    Button, Collapse, NavbarToggler, NavLink,
    Form, FormGroup, Label, Input } from 'reactstrap';

import { Link } from 'react-router-dom';

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",

            isHeaderDropDownOpen: false,
            
        };

        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
    }

    onChangeUsername(e){
        this.setState({
            username: e.target.value, 
        });
    }

    onChangePassword(e){
        this.setState({
            password: e.target.value, 
        });
    }

    AuthIsTrueOrFalseButton() {
        if (this.props.auth.isAuthenticated===true){
            return (
                <Fragment>
                    <NavItem>
                        <Button onClick={() => this.props.userLogout() } > Logout </Button>
                    </NavItem>
                </Fragment>
            );
        } else {
            return (              
                <Fragment>
                    <Form inline>
                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Label for="loginUsername" className="mr-sm-2">Email</Label>
                            <Input type="email" name="loginUsername" id="loginUsername" placeholder="" onChange={this.onChangeUsername} />
                        </FormGroup>
                        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                            <Label for="loginPassword" className="mr-sm-2">Password</Label>
                            <Input type="password" name="loginPassword" id="loginPassword" placeholder="" onChange={this.onChangePassword}/>
                        </FormGroup>
                        {/* <Button onClick={() => this.props.userLogin("admin@aaa.com", "adminadmin") } >Log In</Button> */}
                        <Button onClick={() => this.props.userLogin(this.state.username, this.state.password) } >Log In</Button>
                    </Form>
                </Fragment>
            );
        }
    }
    
    render () {
        return (  
            <Navbar color="light" light expand="md">
                <Container>
                    <Nav navbar>
                        <NavbarBrand className="mr-auto" href="/">
                            <Link to="/">
                                <img src="/favicon.ico" height="30" width="30" alt="Fan Rui" />
                            </Link>
                            <span> ensign Test </span>
                        </NavbarBrand>
                    </Nav>
                    <NavbarToggler onClick={this.props.toggle}/>
                    <Collapse isOpen={this.props.isDropDownOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="/record/"> Record </NavLink>
                            </NavItem>
                            {/* <AuthIsTrueOrFalseButton /> */}
                            {this.AuthIsTrueOrFalseButton()}
                        </Nav>
                    </Collapse>
                </Container>
            </Navbar>
        )
    }
    
}


export default Header;