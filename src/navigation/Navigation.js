import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import Home from "../pages/HomePage";
import Record from '../pages/RecordPage';

import { userLogout, verifyToken, userLogin } from '../redux/ActionCreators';

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        userLogin: (username, password) => { dispatch(userLogin(username, password)) },
        verifyToken: (token) => { dispatch(verifyToken(token)) },
        userLogout: (token) => { dispatch(userLogout(token)) },
    };
}

class Navigation extends Component {
    constructor(props) {
        super(props);
    }

    
    render() {
        return (
            <Switch>
                <Route exact path="/"  component={(props) => <Home {...props} auth={this.props.auth} userLogout={this.props.userLogout} userLogin={this.props.userLogin} />} />
                <Route exact path='/record' component={(props) => <Record {...props} auth={this.props.auth} userLogout={this.props.userLogout} userLogin={this.props.userLogin} />} />
            </Switch> 
        )
    }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Navigation));
