export const LOGIN = 'LOGIN';
export const LOGIN_LOADING = 'LOGIN_LOADING';
export const LOGIN_FAILED = 'LOGIN_FAILED';

export const LOGOUT = 'LOGOUT';
export const LOGOUT_LOADING = 'LOGOUT_LOADING';
export const LOGOUT_FAILED = 'LOGOUT_FAILED';

