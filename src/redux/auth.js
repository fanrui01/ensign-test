import * as ActionTypes from './ActionTypes';

export const Auth = (state = { isLoading: true,
    errMess: null,
    isAuthenticated: false,
    token: null}, action) => {
    
    console.log("Current auth state: ");
    switch (action.type) {
        case ActionTypes.LOGIN:
        console.log({...state, isLoading: false, errMess: null, isAuthenticated: true, token: action.payload});
            return {...state, isLoading: false, errMess: null, isAuthenticated: true, token: action.payload};

        case ActionTypes.LOGIN_LOADING:
            console.log({...state, isLoading: true, errMess: null, isAuthenticated: false, token: null});
            return {...state, isLoading: true, errMess: null, isAuthenticated: false, token: null}

        case ActionTypes.LOGIN_FAILED:
            console.log({...state, isLoading: false, isAuthenticated: false, errMess: action.payload});
            return {...state, isLoading: false, isAuthenticated: false, errMess: action.payload};

        case ActionTypes.LOGOUT:
            console.log({...state, isLoading: false, errMess: null, isAuthenticated: false, token: null});
            return {...state, isLoading: false, errMess: null, isAuthenticated: false, token: null};
    
        case ActionTypes.LOGOUT_LOADING:
            console.log({...state, isLoading: true, errMess: null});
            return {...state, isLoading: true, errMess: null}
    
        case ActionTypes.LOGOUT_FAILED:
            console.log({...state, isLoading: false, errMess: action.payload, isAuthenticated: false, token: null}); 
            return {...state, isLoading: false, errMess: action.payload, isAuthenticated: false, token: null};
        
        default:
            return state;
    }
};