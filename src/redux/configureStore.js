import { createStore, combineReducers, applyMiddleware } from 'redux';

import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native

import { Auth } from './auth';
 
const persistConfig = {
  key: 'root',
  storage,
}
 
const combinedReducer = combineReducers({
                            auth: Auth 
                        });

const persistedReducer = persistReducer(persistConfig, combinedReducer);
 
export const ConfigureStore = () => {
    let store = createStore(persistedReducer, applyMiddleware(thunk, logger));
    let persistor = persistStore(store)

    return {store, persistor};
}

