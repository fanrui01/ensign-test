import auth from './auth';
import records from './record';
import interests from './interest';
import occupations from './occupation';

export default {auth, records, interests, occupations};