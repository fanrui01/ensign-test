import { serverUrl } from '../config';

const list = async (token) => {
    return fetch(serverUrl + '/info/interests')
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                  var error = new Error('Error ' + response.status + ': ' + response.statusText);
                  error.response = response;
                  throw error;
                }
                },
                error => {
                    var errmsg = new Error(error.message);
                    throw errmsg;
                }
            )

    
}


export default { list };



